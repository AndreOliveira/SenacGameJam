﻿using UnityEngine;

public enum ItemType {
	COMPUTADOR,
	ESPELHO,
	LIVROS,
	PORTA,
	TV,
	VIDEOGAME,
	VIOLAO,
}

public enum ItemStatus {
	DEFAULT,
	ACTIVE,
    DISABLED
}

[RequireComponent(typeof(AudioSource))]
public class DistractionItem : MonoBehaviour, IInteractable {

	[Range(0,100)] public int priority = 50;
	[Range(0f,10)] public int time = 5;
//    [Range(0f, 10)] public int disabledTime = 5;
    [SerializeField] private ItemType item;
    [SerializeField] private ItemStatus status = ItemStatus.DEFAULT;
	private AudioSource aSource;
	[SerializeField] private AudioClip usingClip;
	[SerializeField] private AudioClip discouragedClip;
	[SerializeField] private AudioClip tvClip;
	private SpriteRenderer rend;
	public bool beingUsed;

	void Start() {
		aSource = GetComponent<AudioSource> ();
		beingUsed = false;
        GameManager.Instance.human.distractions.Add(this);
		rend = GetComponentInChildren<SpriteRenderer> ();
		Status = ItemStatus.DEFAULT;
		if (tvClip != null) {
			aSource.clip = tvClip;
		}
	}

	public ItemStatus Status {
		get { return status; }
		set {
			this.status = value;
			string spritePath = item + "-" + status;
//			print (name);
			rend.sprite = Resources.Load<Sprite> (spritePath);
		}
    }

	public void BeTarget() {
		Status = ItemStatus.ACTIVE;
	//	Invoke ("DisableItem", time);
	}

	public void DisableItem() {
		Status = ItemStatus.DISABLED;
		beingUsed = false;
		if (tvClip != null || item == ItemType.COMPUTADOR) {
			aSource.Stop ();
			aSource.PlayOneShot (discouragedClip);
		}
	}

    public void NPCInteraction() {
		beingUsed = true;
		if (usingClip != null)
			aSource.PlayOneShot (usingClip);
		if (tvClip != null) {
			Invoke ("PlayTV", 0.5f);
		}
//        Debug.Log("NPM interacted with " + item);
		GameManager.Instance.depressionGauge -= 10f;
		if (GameManager.Instance.depressionGauge < 1) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("Lose");
		}
        // Check if status is active
            // Toggle to active
    }

	void PlayTV() {
		aSource.Play();
	}

	public void PlayerInteraction() {
		if (status == ItemStatus.ACTIVE) {
			if(discouragedClip != null)
				aSource.PlayOneShot (discouragedClip);
			Status = ItemStatus.DISABLED;
			GameManager.Instance.depressionGauge += 10f;
			if (GameManager.Instance.depressionGauge > 75f) {
				UnityEngine.SceneManagement.SceneManager.LoadScene ("Win");
			}
		}
	}
//        if (status == ItemStatus.DISABLED || status == ItemStatus.ACTIVE) return;
//		GameManager.Instance.depressionGauge += 20f;
//		if (GameManager.Instance.depressionGauge < 1) {
//			UnityEngine.SceneManagement.SceneManager.LoadScene ("Win");
//		}
//		SetItemStatus(ItemStatus.DISABLED);
//        Invoke("ResetItemStatus", time);
//    }
//
//    void ResetItemStatus() {
//        SetItemStatus(ItemStatus.DEFAULT);
//    }
}