﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReachHandler : MonoBehaviour
{

    public GameObject target;

	void Update ()
    {
        if (Input.GetKeyDown("space")) this.executeInteractableTargetAction();
    }

    void executeInteractableTargetAction()
    {
        if (this.target != null)
        {
            IInteractable interactableTarget = this.target.GetComponent<IInteractable>();
            if (interactableTarget != null) interactableTarget.PlayerInteraction();

        }
    }

    void OnTriggerEnter(Collider collider)
    {
        this.target = collider.gameObject;
//        Debug.Log("Collided with " + collider.gameObject.name);
    }

    void OnTriggerExit(Collider collider)
    {
        this.target = null;
  //      Debug.Log("Exited collision with " + collider.gameObject.name);
    }
}
