﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanAI : MonoBehaviour {

	[SerializeField] private NavMeshAgent agent;
	[SerializeField] private DistractionItem target;
	[SerializeField] private Transform player;
	[SerializeField] private Transform arts;
	[SerializeField] private LayerMask layerMask;
	[SerializeField] private float distance;
	[SerializeField] private bool isInteracting;
	[Range(0,100)][SerializeField] private int changesProbability = 10;
	private const float maxDistance = 999999f;
	[SerializeField] private Animator anim;
	private Vector3 lateMove;
	public List<DistractionItem> distractions;
	[SerializeField] private SpriteRenderer rend;
	[SerializeField] private float discourageTime;
	private bool discouraged;

	// Use this for initialization
	void Start () {
		discouraged = false;
		lateMove = Vector3.zero;
		isInteracting = false;
		distance = 0f;
		agent = GetComponent<NavMeshAgent> ();
		TargetSelection();
	}
	
	// Update is called once per frame
	void Update () {
		if (!discouraged) {
			if (agent != null && target != null) {
				agent.SetDestination (target.transform.position);
			}
			if (distance != 0f && Vector3.Distance (transform.position, target.transform.position) < distance / 2f && !isInteracting) {
				distance = 0f;
				TargetSelection (); 	
			}
		} else {
			agent.SetDestination (transform.position);
		}
		SendAnimationInfo ();
	}

	void LateUpdate() {
		if (agent.velocity != Vector3.zero) {
			lateMove = agent.velocity;
		}
	}

	void SendAnimationInfo() {
		float idle = 0;
		anim.SetBool ("isMoving", agent.velocity != Vector3.zero);

		float y = transform.rotation.eulerAngles.y;
		float rotationValue = 0f;


		if ((y < -135f && y < -225) || (y > 135f && y < 225f)) {
			//pra baixo
			rotationValue = 0f;
			idle = 0;
		} else if ((y > -45f && y < 45f) || y < -315f || y > 315f) {
			//pra cima
			rotationValue = 1f;
			idle = 1f;
		} else if ((y < 315f && y > 225f) || y < -45f && y > -135f) {
			//pra esquerda
			idle = 0.5f;
			Vector3 v3 = arts.localScale;
			v3.x = -Mathf.Abs(v3.x);
			arts.localScale = v3;
			rotationValue = 0.5f;
		} else {
			Vector3 v3 = arts.localScale;
			idle = 0.5f;
			v3.x = Mathf.Abs(v3.x);
			arts.localScale = v3;
			rotationValue = 0.5f;

		}
		anim.SetFloat ("idle", idle);

		anim.SetFloat ("rotation", rotationValue);


	}

	public void Discourage () {
		StartCoroutine (DiscourageNow ());
	}

	private IEnumerator DiscourageNow() {
		rend.color = new Color (0.5f,0.5f,0.5f,1f);
		discouraged = true;
		yield return new WaitForSeconds (discourageTime);
		discouraged = false;
		rend.color = Color.white;
		TargetSelection ();
	}

	//espero que der certo
	private DistractionItem SelectTarget () {
		int priority = 1;
		for (int i = 0; i < distractions.Count; i++) {
			priority += distractions [i].priority;
		}

		int targetPriority = Random.Range (1, priority);

		for (int i = 0; i < distractions.Count; i++) {
			if (targetPriority <= distractions [i].priority) {
				if (target != distractions [i]) {
					distractions [i].BeTarget ();
					return distractions[i];
				} 
				SelectTarget ();
			} else {
				targetPriority -= distractions [i].priority;
			}
		}//end of for
		return null;
	}//end of select target

	private void TargetSelection() {
		agent.isStopped = false;
		if (target != null) {
			target.DisableItem ();
		}
		target = SelectTarget ();
		distance = (Random.Range (0, 100) < changesProbability) ? Vector3.Distance (transform.position, target.transform.position) : 0f;
	}

	void OnCollisionEnter (Collision coll) {
		if (coll.gameObject.CompareTag ("DistractionItem") && coll.gameObject.GetComponent<DistractionItem>() == target) {
//			target.Status = ItemStatus.ACTIVE;
			agent.isStopped = true;
			target.NPCInteraction();
			isInteracting = true;
			Invoke ("TargetSelection", target.time);
		}
	}

	void OnCollisionExit (Collision coll) {
		if (coll.gameObject.CompareTag ("DistractionItem")) {
			isInteracting = false;		
		}
	}

	void OnTriggerEnter (Collider coll) {
		if (coll.gameObject.CompareTag ("DistractionItem")) {

			//Invoke ("TargetSelection", target.time);
		}
	}

	void OnTriggerStay(Collider coll) {
		if (coll.gameObject.CompareTag ("Player")) {
			RaycastHit hit;

			//if(Physics.Raycast(transform.position,

			if (Physics.Raycast (transform.position, coll.gameObject.transform.position-transform.position, out hit,maxDistance,layerMask)) {
				if (hit.collider != null && hit.collider.CompareTag ("Player")) {
					collided = hit.transform.position;
					GameManager.Instance.depressionGauge -= Time.deltaTime * 10f;
					if (GameManager.Instance.depressionGauge < 1) {
						UnityEngine.SceneManagement.SceneManager.LoadScene ("Lose");					}
				}//if hit player
			}//line cast

		}//if tag player
	}

	Vector3 collided = Vector3.zero;

	void OnDrawGizmos () {
		Gizmos.color = Color.blue;
		if (collided != Vector3.zero) {
			Gizmos.DrawRay (transform.position, collided - transform.position);
		}
	}

}//end of class