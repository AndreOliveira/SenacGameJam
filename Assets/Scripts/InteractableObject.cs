﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour, IInteractable {

    public void NPCInteraction()
    {
        Debug.Log("NPC interacted with " + this.gameObject.name);
    }

    public void PlayerInteraction()
    {
        Debug.Log("Player interacted with " + this.gameObject.name);
    }
}
