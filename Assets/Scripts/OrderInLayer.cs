﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderInLayer : MonoBehaviour {

	Renderer rend;
	public int ord;
	public float offset;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
	}

	void Update() {
		rend.sortingOrder = (int) (-(transform.position.z + offset)* 100f);
		ord = rend.sortingOrder;
	}

}
