﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHumanAI : MonoBehaviour {

	[SerializeField] private Transform human;
	private Vector3 offset = new Vector3 (0f, 0f, 0.95f);

	// Update is called once per frame
	void LateUpdate () {
		transform.position = human.position + offset;
	}
}
