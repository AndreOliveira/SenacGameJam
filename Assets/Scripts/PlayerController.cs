﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    public Canvas UI;

	[Range(0,10)] public float speed;
    public float rotationSpeed = 10.0f;
	private const float speedMultiplier = 100f;

    Rigidbody playerRigidbody;
    // BoxCollider playerBoxCollider;
    Animator animator;
    Vector3 movement, lastDirection;
	[SerializeField] private Transform reach;
	[SerializeField] private AudioSource aSource;
	[SerializeField] private ParticleSystem interaction;
    bool isMoving;
	private DistractionItem target;

    void Awake()
    {
		target = null;
        Time.timeScale = 1.0f;
        playerRigidbody = GetComponent<Rigidbody>();
        // playerBoxCollider = GetComponent<BoxCollider>();
		animator = GetComponentInChildren<Animator>();
        UI.GetComponentInChildren<Canvas>().enabled = false;
		aSource = GetComponent<AudioSource> ();
		if (Application.isMobilePlatform) {
			speed /= 3f;
		}
    }

    public void Resume() {
        UI.GetComponentInChildren<Canvas>().enabled = false;
        Time.timeScale = 1.0f;
    }

    void Update()
    {
		float h = CrossPlatformInputManager.GetAxis("Horizontal") * Time.deltaTime * speed * speedMultiplier;
		float v = CrossPlatformInputManager.GetAxis("Vertical") * Time.deltaTime * speed * speedMultiplier;

		Vector3 vec3 = transform.localScale;
		if (Mathf.Abs(h) > 0.1f) {
			vec3.x = h > 0 ? 1f : -1f;
			transform.localScale = vec3;
		}

        if (Input.GetKeyDown(KeyCode.Escape)) {
            // not the optimal way but for the sake of readability
            if (UI.GetComponentInChildren<Canvas>().enabled)
            {
                UI.GetComponentInChildren<Canvas>().enabled = false;
                Time.timeScale = 1.0f;
            } else {
                UI.GetComponentInChildren<Canvas>().enabled = true;
                Time.timeScale = 0f;
            }
            return;
        }


        isMoving = false;

        if (h < 0 || h > 0 || v < 0 || v > 0) {
            isMoving = true;
            //if(!playerBoxCollider.IsTouchingLayers(-1))
            //if(!playerBoxCollider.IsTouchingLayers(Physics2D.AllLayers)) {
            lastDirection = playerRigidbody.velocity;
            //}
        }

        Move(h, v);

		if (CrossPlatformInputManager.GetButtonDown ("Fire1")) {
			interaction.Play ();
			aSource.PlayOneShot(aSource.clip);
			if (target != null && target.Status == ItemStatus.ACTIVE && !target.beingUsed) {
				target.PlayerInteraction ();
				GameManager.Instance.human.Discourage ();
			}
		}//end if fire1
    }

    void Move(float h, float v)
    {
//        movement.Set(h, 0f, v); 	
//        movement = movement.normalized * speed * Time.deltaTime;
//        Vector3 finalPosition = transform.position + movement;
       // playerRigidbody.position = finalPosition;

        // For some reason, MovePosition drags the movement after the key has been released a bit more than playerRigidbody.movement = blabla
    //    playerRigidbody.MovePosition(finalPosition);
		movement = new Vector3(h,0f,v);
		playerRigidbody.velocity = movement;

//        if (movement != Vector3.zero)
//        {
//            // Strange rotation that works if the Y axis rotation is frozen
//            Quaternion initialRotation = transform.rotation;
//            Quaternion targetRotation = Quaternion.FromToRotation(Vector3.right, movement) * Quaternion.Euler(90f, 0, 0);
//
//            playerRigidbody.MoveRotation(
//                Quaternion.Lerp(
//                    initialRotation,
//                    targetRotation,
//                    Time.deltaTime * speed
//                )
//            );
         
            // Another strange rotation that clips when rotating to an oposing direction
            /*
            playerRigidbody.MoveRotation(
                Quaternion.FromToRotation(Vector3.right, movement) *
                Quaternion.Euler(90f, 0, 0)
            );
            */
//        }

        SendAnimInfo();
		AdjustReachCollider ();
    }

	void AdjustReachCollider() {
		if (Mathf.Abs (playerRigidbody.velocity.z) > Mathf.Abs (playerRigidbody.velocity.x)) {
			if (playerRigidbody.velocity.z > 0) {
				reach.localPosition = new Vector3 (0f, 1f, 0f);
			} else if (playerRigidbody.velocity.z < 0) {
				reach.localPosition = new Vector3 (0f, -1f, 0f);
			}
		} else if (Mathf.Abs (playerRigidbody.velocity.z) < Mathf.Abs (playerRigidbody.velocity.x)) {
				reach.localPosition = new Vector3 (.75f, 0f, 0f);
		}
	}

    void SendAnimInfo() {
        animator.SetFloat("xSpeed", playerRigidbody.velocity.x);
        animator.SetFloat("ySpeed", playerRigidbody.velocity.z);

        animator.SetFloat("lastX", lastDirection.x);
        animator.SetFloat("lastY", lastDirection.z);

        animator.SetBool("isMoving", isMoving);
    }

	void OnTriggerEnter (Collider coll) {
		if (coll.CompareTag ("DistractionItem")) {
			target = coll.GetComponent<DistractionItem>();
		}
	}

	void OnTriggerExit (Collider coll) {
		if (coll.CompareTag ("DistractionItem")) {
			target = null;
		}
	}
}