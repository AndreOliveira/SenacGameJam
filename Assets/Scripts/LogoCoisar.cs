﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class LogoCoisar : MonoBehaviour {

	[SerializeField] private float minTime, maxTime, animationTime;
	[SerializeField] private AudioSource aSource;
	private Animator anim;

	// Use this for initialization
	void Start () {
		aSource = GetComponent<AudioSource> ();
		anim = GetComponent<Animator> ();
		StartCoroutine (Coisar ());
	}
	
	IEnumerator Coisar() {
		while (true) {
			yield return new WaitForSeconds (Random.Range (minTime, maxTime));
			anim.SetBool ("Coisar", true);
			aSource.Play ();
			yield return new WaitForSeconds (animationTime);
			anim.SetBool ("Coisar", false);
		}
	}
}
