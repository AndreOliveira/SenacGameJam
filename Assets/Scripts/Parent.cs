﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Parent : MonoBehaviour {

	[SerializeField] private NavMeshAgent agent;
	[SerializeField] private List<Transform> path;
	private int step;

	void Awake() {
		if (path.Count == 0) {
			Debug.LogError ("The path is empty");
		}
	}

	// Use this for initialization
	void Start () {
		step = 0;
	}
	
	// Update is called once per frame
	void Update () {
		agent.SetDestination (path [step < path.Count ? step : 0].position);
	}
}
