﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	private static GameManager instance;

	[Range(0,100)] public float depressionGauge = 50f;
	public HumanAI human;
	public GameObject parents;
	public Image blackScreen;

	public static GameManager Instance {
		get {
			return instance != null ? instance : instance = FindObjectOfType<GameManager> ();
		}
	}

	void Update () {
		blackScreen.color = new Color (0f, 0f, 0f, depressionGauge / 100f);
	}
}